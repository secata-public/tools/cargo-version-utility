# Cargo Version Utility

This is a small cargo subcommand `cargo version-util` to manipulate `Cargo.toml` versions.
It handles workspaces transparently.

## Installation

``` shell
cargo install --path .
```

## Usage

### `get-version`

This subcommand gets the version of the specified `Cargo.toml` file.

``` shell
cargo version-util get-version [--manifest-path /path/to/Cargo.toml]
```

*Note:* If run in a workspace and member crates have distinct versions the `get-version` command fails with an error.

### `set-version`

This subcommand sets the version for the specified `Cargo.toml` file.

``` shell
cargo version-util set-version [--manifest-path /path/to/Cargo.toml] <VERSION>
```

If run in a workspace all members get the same version.

### `is-workspace`

This subcommand checks if the specified `Cargo.toml` is a workspace toml.

``` shell
cargo version-util is-workspace [--manifest-path /path/to/Cargo.toml]
```
