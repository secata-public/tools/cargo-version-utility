//! This is small Cargo subcommand to manipulate Cargo.toml versions.

use cargo_metadata::{Metadata, Package};
use std::error::Error;
use std::path::{Path, PathBuf};
use std::{fs, process};
use toml_edit::{value, Document};

use clap::{Parser, Subcommand};

#[derive(Parser)]
#[clap(name = "cargo")]
#[clap(bin_name = "cargo")]
pub(crate) enum Cargo {
    VersionUtil(Arguments),
}

#[derive(clap::Args)]
#[clap(
    author,
    version,
    long_about = "\nCargo Version Utility\nManipulates Cargo.toml versions."
)]
struct Arguments {
    #[clap(subcommand)]
    commands: Commands,
}

#[derive(Subcommand)]
enum Commands {
    #[clap(about = "Sets the version on a Cargo project.")]
    SetVersion {
        #[clap(help = "The new version.")]
        version: String,
        #[clap(help = "The path to the Cargo.toml to edit.", long = "manifest-path")]
        manifest_path: Option<PathBuf>,
    },

    #[clap(about = "Gets the version of a Cargo project.")]
    GetVersion {
        #[clap(help = "The path to the Cargo.toml to query.", long = "manifest-path")]
        manifest_path: Option<PathBuf>,
    },

    #[clap(about = "Determines if the Cargo.toml is a workspace, exits with exit code 0 if true.")]
    IsWorkspace {
        #[clap(help = "The path to the Cargo.toml to query.", long = "manifest-path")]
        manifest_path: Option<PathBuf>,
    },
}

fn main() -> Result<(), Box<dyn Error>> {
    let Cargo::VersionUtil(arguments) = Cargo::parse();

    run_command(arguments)
}

fn run_command(arguments: Arguments) -> Result<(), Box<dyn Error>> {
    let default_path: PathBuf = "Cargo.toml".to_string().into();
    match arguments.commands {
        Commands::SetVersion {
            version,
            manifest_path,
        } => {
            let path = manifest_path.unwrap_or(default_path);
            set_version(&version, &path)
        }
        Commands::GetVersion { manifest_path } => {
            let path = manifest_path.unwrap_or(default_path);
            get_version(&path)
        }
        Commands::IsWorkspace { manifest_path } => {
            let path = manifest_path.unwrap_or(default_path);
            let status = if is_workspace(&path) { 0 } else { 1 };
            process::exit(status);
        }
    }
}

struct PathAndDoc {
    manifest_path: PathBuf,
    package: Option<Package>,
    toml: Document,
}

/// This creates a `Vec` containing the metadata and manifest of the root project.
/// If run on a workspace the result contains metadata and manifests of all workspace members.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
/// ### Returns:
/// [`Vec<PathAndDoc>`] for all the crates
///
fn read_package_manifests(manifest_path: &Path) -> Vec<PathAndDoc> {
    let metadata = get_metadata(manifest_path);
    let metadata_packages: Vec<&Package> = if is_workspace(manifest_path) {
        metadata.workspace_packages()
    } else {
        metadata.packages.iter().collect()
    };

    metadata_packages
        .into_iter()
        .map(read_package_manifest)
        .collect()
}

fn read_package_manifest(package: &Package) -> PathAndDoc {
    let mut path_and_doc = read_package_toml(package.manifest_path.as_std_path());
    path_and_doc.package = Some(package.clone());
    path_and_doc
}

fn read_package_toml(package_manifest_path: &Path) -> PathAndDoc {
    let toml = read_toml_as_document(package_manifest_path).unwrap();
    PathAndDoc {
        manifest_path: package_manifest_path.to_owned(),
        package: None,
        toml,
    }
}

/// Sets the specified version on the manifest at the given path.
///
/// ### Parameters:
///
/// * `version`: The version to set
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn set_version(version: &str, manifest_path: &Path) -> Result<(), Box<dyn Error>> {
    let package_manifests = read_package_manifests(manifest_path);
    for path_and_doc in package_manifests.into_iter() {
        update_version_and_write(version, path_and_doc)?;
    }

    if is_workspace(manifest_path) {
        let path_and_doc = read_package_toml(manifest_path);
        update_version_and_write_for_workspace_cargo_toml(version, path_and_doc)?;
    }

    update_cargo_lock_with_cargo_update(manifest_path)?;

    Ok(())
}

fn update_cargo_lock_with_cargo_update(manifest_path: &Path) -> Result<(), Box<dyn Error>> {
    let path_buf = manifest_path.canonicalize()?.to_path_buf();
    let manifest_directory = path_buf.parent().unwrap();

    let mut cargo_lock = path_buf.parent().unwrap().to_path_buf();
    cargo_lock.push("Cargo.lock");

    if cargo_lock.exists() {
        std::process::Command::new("cargo")
            .args(["update", "-q", "-w", "--offline"])
            .current_dir(manifest_directory)
            .status()?;
    }

    Ok(())
}

fn update_version_and_write(version: &str, path_and_doc: PathAndDoc) -> Result<(), Box<dyn Error>> {
    let mut doc = path_and_doc.toml;

    // Ignore doc if version is imported from the workspace.
    let is_version_workspace = doc["package"]["version"]
        .get("workspace")
        .and_then(|x| x.as_bool())
        .unwrap_or(false);
    if !is_version_workspace {
        doc["package"]["version"] = value(version);
        fs::write(&path_and_doc.manifest_path, doc.to_string().as_bytes())?;
    }
    Ok(())
}

fn update_version_and_write_for_workspace_cargo_toml(
    version: &str,
    path_and_doc: PathAndDoc,
) -> Result<(), Box<dyn Error>> {
    let mut doc = path_and_doc.toml;
    // Ignore doc if version is imported from the workspace.
    let version_exists: bool = doc
        .get("workspace")
        .and_then(|x| x.get("package"))
        .and_then(|x| x.get("version"))
        .is_some();
    if version_exists {
        doc["workspace"]["package"]["version"] = value(version);
        fs::write(&path_and_doc.manifest_path, doc.to_string().as_bytes())?;
    }
    Ok(())
}

/// Gets the version from the manifest at the given path and prints it to std out.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn get_version(manifest_path: &Path) -> Result<(), Box<dyn Error>> {
    let package_manifests = read_package_manifests(manifest_path);
    let mut versions: Vec<String> = package_manifests
        .iter()
        .map(|path_and_doc| path_and_doc.package.as_ref().unwrap().version.to_string())
        .collect();

    versions.sort();
    versions.dedup();

    if versions.len() != 1 {
        eprintln!("More than one unique version in the manifest/workspace");
        for path_and_doc in package_manifests.iter() {
            eprintln!(
                "{}: {}",
                path_and_doc.manifest_path.display(),
                path_and_doc.package.as_ref().unwrap().version
            );
        }

        process::exit(1);
    } else {
        println!("{}", versions[0]);
        Ok(())
    }
}

/// Returns [`true`] if given manifest is a workspace manifest, [`false`] otherwise.
///
/// ### Parameters:
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
fn is_workspace(manifest_path: &Path) -> bool {
    let toml = read_toml_as_document(manifest_path).unwrap();
    toml.contains_table("workspace")
}

fn read_toml_as_document(toml_path: &Path) -> Result<Document, Box<dyn Error>> {
    let file_content = fs::read_to_string(toml_path)?;
    match file_content.parse::<Document>() {
        Ok(it) => Ok(it),
        Err(err) => Err(err.into()),
    }
}

/// Get metadata for the current crate.
///
/// ### Parameters
///
/// * `manifest_path`: The Cargo.toml manifest path.
///
/// ### Returns:
///
/// [`Metadata`] for the crate.
///
fn get_metadata(manifest_path: &Path) -> Metadata {
    let mut cmd = cargo_metadata::MetadataCommand::new();
    cmd.manifest_path(manifest_path);
    cmd.no_deps();
    let result = cmd.exec();
    if let Ok(metadata) = result {
        metadata
    } else {
        panic!("{}", result.err().unwrap());
    }
}
